package kz.damir021;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class GUI extends JFrame {
    private JButton button = new JButton("Зашифровать");
    private JButton button2 = new JButton("Расшифровать");
    private JTextArea input = new JTextArea();
    private JTextArea input2 = new JTextArea();
    private JTextField input3 = new JTextField("");
    private JLabel label = new JLabel("Исходный текст:");
    private JLabel label2 = new JLabel("Конечный текст:");
    private JLabel label3 = new JLabel("Сдвиг (1-25):");
    private JPanel mainPannel = new JPanel();
    private JPanel textAreaPannel = new JPanel();
    private JPanel textAreaPannel2 = new JPanel();
    private JPanel textField = new JPanel();
    private JPanel buttonsPannelAll = new JPanel();
    private JPanel buttonsPannel = new JPanel();
    private JPanel buttonsPannel2 = new JPanel();



    public GUI () {
        super("Программа шифрования цезаря");
        Image icon = new ImageIcon(this.getClass().getResource("/logo/logo.png")).getImage();
        this.setIconImage(icon);


        this.setBounds(100,100,500,500);
        this.setMinimumSize(new Dimension(500, 500));
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        input.setLineWrap(true);
        input.setWrapStyleWord(true);

        input2.setLineWrap(true);
        input2.setWrapStyleWord(true);

        button.addActionListener(new EncryptEvent());
        button2.addActionListener(new DecryptEvent());
        mainPannel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        mainPannel.setLayout(new GridLayout(0,1));

        input.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        input2.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        input3.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        input2.setEditable(false);


        textAreaPannel.setBorder(BorderFactory.createEmptyBorder(10, 0, 10, 0));
        textAreaPannel.setLayout(new GridLayout(0,2));
        textAreaPannel.add(label);
        textAreaPannel.add(input);
        JScrollPane inputScroll = new JScrollPane(input, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        textAreaPannel.add(inputScroll);

        textAreaPannel2.setBorder(BorderFactory.createEmptyBorder(10, 0, 10, 0));
        textAreaPannel2.setLayout(new GridLayout(0,2));
        textAreaPannel2.add(label2);
        textAreaPannel2.add(input2);
        JScrollPane inputScroll2 = new JScrollPane(input2, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        textAreaPannel2.add(inputScroll2);

        textField.setBorder(BorderFactory.createEmptyBorder(10, 0, 10, 0));
        textField.setLayout(new GridLayout(1,2));
        textField.add(label3);
        textField.add(input3);

        buttonsPannel.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 0));
        buttonsPannel.setLayout(new GridLayout(1,1));

        buttonsPannel2.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 0));
        buttonsPannel2.setLayout(new GridLayout(1,1));

        buttonsPannelAll.setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 0));
        buttonsPannelAll.setLayout(new GridLayout(3,2));


        buttonsPannel.add(button);
        buttonsPannel2.add(button2);

        buttonsPannelAll.add(textField);
        buttonsPannelAll.add(buttonsPannel);
        buttonsPannelAll.add(buttonsPannel2);


        mainPannel.add(textAreaPannel);
        mainPannel.add(textAreaPannel2);
        mainPannel.add(buttonsPannelAll);


        Container container = this.getContentPane();

        container.add(mainPannel);



    }

    Caesar caesarMethods = new Caesar();


    class EncryptEvent implements ActionListener {
        public void actionPerformed (ActionEvent e){
            String text = input.getText();
            int key = 0;
            if(text.length() == 0){
                JOptionPane.showMessageDialog(null, "Введите текст", "Error", JOptionPane.PLAIN_MESSAGE);
                return;
            }
            if(isNumeric(input3.getText())){
                key = Integer.parseInt(input3.getText());
            }else{
                JOptionPane.showMessageDialog(null, "Введите сдвиг в виде числа от 1 до 25", "Error", JOptionPane.PLAIN_MESSAGE);
                return;
            }
            caesarMethods.caesarText(text, key, false);
            input2.setText(caesarMethods.outPutText);
        }
    }
    class DecryptEvent implements ActionListener {
        public void actionPerformed (ActionEvent e){
            String text = input.getText();
            int key = 0;
            if(text.length() == 0){
                JOptionPane.showMessageDialog(null, "Введите текст", "Error", JOptionPane.PLAIN_MESSAGE);
                return;
            }
            if(isNumeric(input3.getText())){
                key = Integer.parseInt(input3.getText());
            }else{
                JOptionPane.showMessageDialog(null, "Введите сдвиг в виде числа от 1 до 25", "Error", JOptionPane.PLAIN_MESSAGE);
                return;
            }
            caesarMethods.caesarText(text, key, true);
            input2.setText(caesarMethods.outPutText);
        }
    }
    public static boolean isNumeric(String strNum) {
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }
}
