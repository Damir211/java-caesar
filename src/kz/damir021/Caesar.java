package kz.damir021;

public class Caesar {
    public String outPutText = "";

    public int ruBigIndexStart = 1040;
    public int ruBigIndexEnd = 1071;

    public int ruIndexStart = 1072;
    public int ruIndexEnd = 1103;

    public int engBigIndexStart = 65;
    public int engBigIndexEnd = 90;

    public int engIndexStart = 97;
    public int engIndexEnd = 122;

    public void caesarText(String text, int key, boolean isDecript) {
        char[] myArray = new char[text.length()];
        outPutText = "";
        for (int i = 0; i < text.length(); i++){
            char symbolChar = text.charAt(i);
            int symbolNumber = (int)symbolChar;

            if(ruBigIndexStart <= symbolNumber && symbolNumber <= ruBigIndexEnd){
                symbolNumber = shift(symbolNumber, key, isDecript, ruBigIndexStart, ruBigIndexEnd);
            }else if(ruIndexStart <= symbolNumber && symbolNumber <= ruIndexEnd){
                symbolNumber = shift(symbolNumber, key, isDecript, ruIndexStart, ruIndexEnd);
            }else if(engBigIndexStart <= symbolNumber && symbolNumber <= engBigIndexEnd){
                symbolNumber = shift(symbolNumber, key, isDecript, engBigIndexStart, engBigIndexEnd);
            }else if(engIndexStart <= symbolNumber && symbolNumber <= engIndexEnd){
                symbolNumber = shift(symbolNumber, key, isDecript, engIndexStart, engIndexEnd);
            }
            outPutText += (char)symbolNumber;
        }
    }
    public int shift(int charNumber, int key, boolean isDecript, int startSymbol, int endSymbol){
        int charNumberIndex = charNumber;
        if(!isDecript){
            if((charNumberIndex + key) > endSymbol){
                charNumberIndex += key;
                charNumberIndex = (charNumberIndex % startSymbol) + (startSymbol - 1);
            }else{
                charNumberIndex += key;
            }
        }else{
            if((charNumberIndex - key) < startSymbol){
                charNumberIndex -= key;
                charNumberIndex += (endSymbol - startSymbol + 1);
            }else{
                charNumberIndex -= key;
            }
        }
        return charNumberIndex;
    }
}
